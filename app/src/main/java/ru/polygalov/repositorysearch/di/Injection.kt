package ru.polygalov.repositorysearch.di

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import ru.polygalov.repositorysearch.data.DetailsRepository
import ru.polygalov.repositorysearch.data.local.GithubLocalCache
import ru.polygalov.repositorysearch.data.local.RepositoryDatabase
import ru.polygalov.repositorysearch.data.remote.GithubService
import ru.polygalov.repositorysearch.viewmodel.ViewModelFactory
import ru.polygalov.repositorysearch.data.MainRepository
import ru.polygalov.repositorysearch.data.local.UserDatabase
import ru.polygalov.repositorysearch.data.local.UserReposDatabase
import ru.polygalov.repositorysearch.viewmodel.DetailsViewModelFactory
import java.util.concurrent.Executors

object Injection {

    private fun provideCache(context: Context): GithubLocalCache {
        val database = RepositoryDatabase.getInstance(context)
        val databaseUser = UserDatabase.getInstance(context)
        val databaseUserRepos = UserReposDatabase.getInstance(context)
        return GithubLocalCache(database.repositoryDao(), databaseUser.userDao(), databaseUserRepos.userReposDao(), Executors.newSingleThreadExecutor())
    }

    private fun provideGithubRepository(context: Context): MainRepository {
        return MainRepository(GithubService.create(), provideCache(context))
    }

    private fun provideUserRepository(context: Context): DetailsRepository {
        return DetailsRepository(GithubService.create(), provideCache(context))
    }

    fun provideViewModelFactory(context: Context): ViewModelProvider.Factory {
        return ViewModelFactory(provideGithubRepository(context))
    }

    fun provideDetailsViewModelFactory(context: Context): ViewModelProvider.Factory {
        return DetailsViewModelFactory(provideUserRepository(context))
    }
}