package ru.polygalov.repositorysearch.ui.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import ru.polygalov.repositorysearch.data.DetailsRepository
import ru.polygalov.repositorysearch.data.local.entity.UserReposResult
import ru.polygalov.repositorysearch.data.local.entity.UserResult
import ru.polygalov.repositorysearch.data.remote.UserRepos
import ru.polygalov.repositorysearch.data.remote.UserResponse

class DetailsViewModel(private val repository: DetailsRepository) : ViewModel() {

    private val queryLiveData = MutableLiveData<String>()
    private val queryUserReposData = MutableLiveData<String>()
    private val userResult: LiveData<UserResult> = Transformations.map(queryLiveData) {
        repository.searchUser(it)
    }

    private val usersReposResult: LiveData<UserReposResult> = Transformations.map(queryUserReposData) {
        repository.searchUsersRepos(it)
    }

    fun searchUser(queryString: String) {
        queryLiveData.postValue(queryString)
    }

    fun searchUsersRepositories(queryString: String) {
        queryUserReposData.postValue(queryString)
    }

    val user: LiveData<UserResponse> = Transformations.switchMap(userResult) { it -> it.data }
    val userRepos: LiveData<List<UserRepos>> = Transformations.switchMap(usersReposResult) { it -> it.data }
    val networkErrors: LiveData<String> = Transformations.switchMap(userResult) { it ->
        it.networkErrors
    }
}