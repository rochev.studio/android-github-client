package ru.polygalov.repositorysearch.ui.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.polygalov.repositorysearch.R
import ru.polygalov.repositorysearch.data.remote.UserRepos
import java.text.SimpleDateFormat
import java.util.*

class UserRepositoryViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val name: TextView = view.findViewById(R.id.repo_name)
    private val language: TextView = view.findViewById(R.id.repo_language)
    private val repoUpdated: TextView = view.findViewById(R.id.repo_updated)
    private val starsCount: TextView = view.findViewById(R.id.stars)
    private val info: ImageView = view.findViewById(R.id.info)
    private val detailsLayout: LinearLayout = view.findViewById(R.id.details_layout)

    private var repo: UserRepos? = null

    fun bind(repo: UserRepos?) {
        if (repo == null) {
            val resources = itemView.resources
            name.text = resources.getString(R.string.loading)
        } else {
            showRepoData(repo)
        }
    }

    private fun showRepoData(repo: UserRepos) {
        this.repo = repo
        name.text = repo.name
        val inputFormat =
            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())
        val outputFormat =
            SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
        val date = inputFormat.parse(repo.updated_at)
        val formattedDate = outputFormat.format(date)
        repoUpdated.text = String.format("updated %1s", formattedDate)
        starsCount.text = repo.stargazers_count
        language.text = repo.language
    }

    fun showDetails() {
        detailsLayout.visibility = View.VISIBLE
        info.visibility = View.GONE
    }

    fun hideDetails() {
        detailsLayout.visibility = View.GONE
        info.visibility = View.VISIBLE
    }

    companion object {
        fun create(parent: ViewGroup): UserRepositoryViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_user_repository, parent, false)
            return UserRepositoryViewHolder(
                view
            )
        }
    }
}