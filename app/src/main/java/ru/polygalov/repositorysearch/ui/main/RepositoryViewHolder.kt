package ru.polygalov.repositorysearch.ui.main

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ru.polygalov.repositorysearch.R
import ru.polygalov.repositorysearch.data.local.entity.Repository
import ru.polygalov.repositorysearch.ui.details.DetailsActivity

class RepositoryViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val name: TextView = view.findViewById(R.id.user_name)
    private val type: TextView = view.findViewById(R.id.type)
    private val avatar: ImageView = view.findViewById(R.id.user_avatar)

    private var repo: Repository? = null

    fun bind(repo: Repository?) {
        if (repo == null) {
            val resources = itemView.resources
            name.text = resources.getString(R.string.loading)
            avatar.visibility = View.GONE
        } else {
            showRepoData(repo)
        }
    }

    private fun showRepoData(repo: Repository) {
        this.repo = repo
        name.text = repo.login
        type.text = repo.type
        itemView.setOnClickListener {
            val intent = Intent(itemView.context, DetailsActivity::class.java).putExtra(
                "user_name", repo.login.toString()
            )
            itemView.context.startActivity(intent)
        }

        Glide.with(itemView.context)
            .load(repo.avatarUrl)
            .circleCrop()
            .into(avatar)

    }

    companion object {
        fun create(parent: ViewGroup): RepositoryViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_repository, parent, false)
            return RepositoryViewHolder(
                view
            )
        }
    }
}