package ru.polygalov.repositorysearch.ui.details

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_details.*
import ru.polygalov.repositorysearch.R
import ru.polygalov.repositorysearch.data.remote.UserRepos
import ru.polygalov.repositorysearch.data.remote.UserResponse
import ru.polygalov.repositorysearch.di.Injection
import java.text.SimpleDateFormat
import java.util.*

class DetailsActivity : AppCompatActivity() {

    private lateinit var viewModel: DetailsViewModel
    private val adapter =
        UserRepositoryAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        viewModel = ViewModelProviders.of(this, Injection.provideDetailsViewModelFactory(this))
            .get(DetailsViewModel::class.java)
        viewModel.searchUser(intent.getStringExtra("user_name").toString())
        viewModel.searchUsersRepositories(intent.getStringExtra("user_name").toString())

        initView()
        initAdapter()
    }

    private fun initView() {
        viewModel.user.observe(this, Observer<UserResponse> {
            if (it != null) {
                Log.d("Activity", "list: ${it.login}")
                name.text = it.login
                val inputFormat =
                    SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())
                val outputFormat =
                    SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
                val date = inputFormat.parse(it.created_at)
                val formattedDate = outputFormat.format(date)
                created.text = formattedDate
                location.text = it.location

                Glide.with(this)
                    .load(it.avatar_url)
                    .circleCrop()
                    .into(user_avatar_details)
            }

        })
        viewModel.networkErrors.observe(this, Observer<String> {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })
    }

    private fun initAdapter() {
        repos_list.adapter = adapter
        viewModel.userRepos.observe(this, Observer<List<UserRepos>> {
            Log.d("Activity", "list: ${it?.size}")
            showEmptyList(it?.size == 0)
            adapter.submitList(it)
        })
        viewModel.networkErrors.observe(this, Observer<String> {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })
    }

    private fun showEmptyList(show: Boolean) {
        if (show) {
            empty_user_repos_list.visibility = View.VISIBLE
            repos_list.visibility = View.GONE
        } else {
            empty_user_repos_list.visibility = View.GONE
            repos_list.visibility = View.VISIBLE
        }
    }
}