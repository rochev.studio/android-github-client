package ru.polygalov.repositorysearch.ui.details

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ru.polygalov.repositorysearch.data.remote.UserRepos

class UserRepositoryAdapter : ListAdapter<UserRepos, RecyclerView.ViewHolder>(REPO_COMPARATOR) {

    private var rowindex = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return UserRepositoryViewHolder.create(
            parent
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val repoItem = getItem(position)
        if (repoItem != null) {
            (holder as UserRepositoryViewHolder).bind(repoItem)
        }
        holder.itemView.setOnClickListener {
            rowindex = position
            notifyDataSetChanged()

        }
        if (rowindex == position) {
            (holder as UserRepositoryViewHolder).showDetails()
        } else {
            (holder as UserRepositoryViewHolder).hideDetails()
        }

    }

    companion object {
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<UserRepos>() {
            override fun areItemsTheSame(oldItem: UserRepos, newItem: UserRepos): Boolean =
                oldItem.name == newItem.name

            override fun areContentsTheSame(oldItem: UserRepos, newItem: UserRepos): Boolean =
                oldItem == newItem
        }
    }
}
