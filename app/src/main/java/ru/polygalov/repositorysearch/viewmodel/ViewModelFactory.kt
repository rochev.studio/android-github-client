package ru.polygalov.repositorysearch.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.polygalov.repositorysearch.data.DetailsRepository
import ru.polygalov.repositorysearch.data.MainRepository
import ru.polygalov.repositorysearch.ui.details.DetailsViewModel
import ru.polygalov.repositorysearch.ui.main.MainViewModel


class ViewModelFactory(private val repository: MainRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return MainViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

class DetailsViewModelFactory(private val user: DetailsRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DetailsViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return DetailsViewModel(user) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
