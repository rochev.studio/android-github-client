package ru.polygalov.repositorysearch.data.remote

import androidx.room.*
import com.google.gson.annotations.SerializedName

@Entity(tableName = "user_repos")
data class UserRepos (

    @PrimaryKey @field:SerializedName("id") val total: Long,
    @field:SerializedName("name") val name: String,
    @field:SerializedName("full_name") val full_name: String,
    @field:SerializedName("stargazers_count") val stargazers_count: String,
    @field:SerializedName("updated_at") val updated_at: String,
    @field:SerializedName("language") val language: String?

)