package ru.polygalov.repositorysearch.data.local

import android.util.Log
import androidx.lifecycle.LiveData
import ru.polygalov.repositorysearch.data.local.entity.Repository
import ru.polygalov.repositorysearch.data.remote.UserRepos
import ru.polygalov.repositorysearch.data.remote.UserResponse
import java.util.concurrent.Executor

class GithubLocalCache(
    private val repoDao: RepositoryDao,
    private val userDao: UserDao,
    private val userReposDao: UserReposDao,
    private val ioExecutor: Executor
) {
    fun insert(repos: List<Repository>, insertFinished: () -> Unit) {
        ioExecutor.execute {
            Log.d("GithubLocalCache", "inserting ${repos.size} users")
            repoDao.insert(repos)
            insertFinished()
        }
    }

    fun reposByName(name: String): LiveData<List<Repository>> {
        // appending '%' so we can allow other characters to be before and after the query string
        val query = "%${name.replace(' ', '%')}%"
        return repoDao.reposByName(query)
    }

    fun insertUser(repos: UserResponse, insertFinished: () -> Unit) {
        ioExecutor.execute {
//            if (repos.location == null) {
//                repos.location == ""
//
//            }
            userDao.insertUser(repos)
            insertFinished()
        }
    }

    fun getUser(name: String): LiveData<UserResponse> {
        // appending '%' so we can allow other characters to be before and after the query string
        val query = "%${name.replace(' ', '%')}%"
        return userDao.getUser(query)
    }

    fun insertUserRepos(repos: List<UserRepos>, insertFinished: () -> Unit) {
        ioExecutor.execute {
            Log.d("GithubLocalCache", "inserting ${repos.size} repos")

            for ((index, userRepos) in repos.withIndex()) {
                println("the element at $index is $userRepos")
            }
            userReposDao.insertUserRepos(repos)
            insertFinished()
        }
    }

    fun getUserRepos(name: String): LiveData<List<UserRepos>> {
        // appending '%' so we can allow other characters to be before and after the query string
        val query = "%${name.replace(' ', '%')}%"
        return userReposDao.getUserRepos(query)
    }
}