package ru.polygalov.repositorysearch.data.local.entity

import androidx.lifecycle.LiveData
import ru.polygalov.repositorysearch.data.remote.UserRepos

data class UserReposResult (
    val data: LiveData<List<UserRepos>>,
    val networkErrors: LiveData<String>
)