package ru.polygalov.repositorysearch.data.remote

import com.google.gson.annotations.SerializedName
import ru.polygalov.repositorysearch.data.local.entity.Repository

data class RepositoryResponse(
    @SerializedName("total_count") val total: Int = 0,
    @SerializedName("items") val items: List<Repository> = emptyList(),
    val nextPage: Int? = null
)