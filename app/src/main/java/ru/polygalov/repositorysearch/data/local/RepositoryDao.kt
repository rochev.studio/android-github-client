package ru.polygalov.repositorysearch.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ru.polygalov.repositorysearch.data.local.entity.Repository
import ru.polygalov.repositorysearch.data.local.entity.UserResult
import ru.polygalov.repositorysearch.data.remote.UserResponse

@Dao
interface RepositoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(posts: List<Repository>)

    @Query("SELECT * FROM users WHERE (login LIKE :queryString) OR (login LIKE " +
            ":queryString)")
    fun reposByName(queryString: String): LiveData<List<Repository>>
}