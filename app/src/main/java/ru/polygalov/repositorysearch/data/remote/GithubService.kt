package ru.polygalov.repositorysearch.data.remote

import android.util.Log
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import ru.polygalov.repositorysearch.data.local.entity.Repository

private const val TAG = "GithubService"
private const val IN_QUALIFIER = "in:name,description"

fun searchUsers(
    service: GithubService,
    query: String,
    page: Int,
    itemsPerPage: Int,
    onSuccess: (repos: List<Repository>) -> Unit,
    onError: (error: String) -> Unit
) {
    Log.d(TAG, "query: $query, page: $page, itemsPerPage: $itemsPerPage")

    service.getSearchUsers(query, page, itemsPerPage).enqueue(
        object : Callback<RepositoryResponse> {
            override fun onFailure(call: Call<RepositoryResponse>?, t: Throwable) {
                Log.d(TAG, "fail to get data")
                onError(t.message ?: "unknown error")
            }

            override fun onResponse(
                call: Call<RepositoryResponse>?,
                response: Response<RepositoryResponse>
            ) {
                Log.d(TAG, "got a response $response")
                if (response.isSuccessful) {
                    val repos = response.body()?.items ?: emptyList()
                    onSuccess(repos)
                } else {
                    onError(response.errorBody()?.string() ?: "Unknown error")
                }
            }
        }
    )
}

fun getUser(
    service: GithubService,
    username: String,
    onSuccess: (user: UserResponse) -> Unit,
    onError: (error: String) -> Unit
) {

    service.getUser(username).enqueue(
        object : Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>?, t: Throwable) {
                Log.d(TAG, "fail to get data")
                onError(t.message ?: "unknown error")
            }

            override fun onResponse(
                call: Call<UserResponse>?,
                response: Response<UserResponse>
            ) {
                Log.d(TAG, "got a response $response")
                if (response.isSuccessful) {
                    val user = response.body()
                    onSuccess(user!!)
                } else {
                    onError(response.errorBody()?.string() ?: "Unknown error")
                }
            }
        }
    )
}

fun getUserRepos(
    service: GithubService,
    username: String,
    onSuccess: (user: List<UserRepos>) -> Unit,
    onError: (error: String) -> Unit
) {

    service.getUserRepos(username).enqueue(
        object : Callback<List<UserRepos>> {
            override fun onFailure(call: Call<List<UserRepos>>?, t: Throwable) {
                Log.d(TAG, "fail to get data")
                onError(t.message ?: "unknown error")
            }

            override fun onResponse(
                call: Call<List<UserRepos>>?,
                response: Response<List<UserRepos>>
            ) {
                Log.d(TAG, "got a response $response")
                if (response.isSuccessful) {
                    val user = response.body()
                    onSuccess(user!!)
                } else {
                    onError(response.errorBody()?.string() ?: "Unknown error")
                }
            }
        }
    )
}

interface GithubService {

    @GET("search/users?")
    fun getSearchUsers(
        @Query("q") query: String,
        @Query("page") page: Int,
        @Query("per_page") itemsPerPage: Int
    ) : Call<RepositoryResponse>

    @GET("users/{username}")
    fun getUser(
        @Path("username") username: String
    ) : Call<UserResponse>

    @GET("users/{username}/repos")
    fun getUserRepos(
        @Path("username") username: String
    ) : Call<List<UserRepos>>

    companion object {
        private const val BASE_URL = "https://api.github.com/"

        fun create(): GithubService {
            val logger = HttpLoggingInterceptor()
            logger.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(GithubService::class.java)
        }
    }
}