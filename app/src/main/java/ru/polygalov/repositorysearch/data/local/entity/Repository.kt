package ru.polygalov.repositorysearch.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "users")
data class Repository(
    @PrimaryKey @field:SerializedName("id") val id: Long,
    @field:SerializedName("login") val login: String?,
    @field:SerializedName("avatar_url") val avatarUrl: String?,
    @field:SerializedName("type") val type: String?
)

