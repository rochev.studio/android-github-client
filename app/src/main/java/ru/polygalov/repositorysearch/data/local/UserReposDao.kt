package ru.polygalov.repositorysearch.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ru.polygalov.repositorysearch.data.local.entity.Repository
import ru.polygalov.repositorysearch.data.local.entity.UserResult
import ru.polygalov.repositorysearch.data.remote.UserRepos
import ru.polygalov.repositorysearch.data.remote.UserResponse

@Dao
interface UserReposDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUserRepos(posts: List<UserRepos>)

    @Query("SELECT * FROM user_repos WHERE full_name LIKE :queryString")
    fun getUserRepos(queryString: String): LiveData<List<UserRepos>>
}