package ru.polygalov.repositorysearch.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import ru.polygalov.repositorysearch.data.local.entity.Repository
import ru.polygalov.repositorysearch.data.remote.UserRepos
import ru.polygalov.repositorysearch.data.remote.UserResponse

@Database(
    entities = [UserRepos::class],
    version = 1,
    exportSchema = false
)
abstract class UserReposDatabase : RoomDatabase() {

    abstract fun userReposDao(): UserReposDao

    companion object {

        @Volatile
        private var INSTANCE: UserReposDatabase? = null

        fun getInstance(context: Context): UserReposDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext,
                UserReposDatabase::class.java, "user_repos.db")
                .build()
    }
}