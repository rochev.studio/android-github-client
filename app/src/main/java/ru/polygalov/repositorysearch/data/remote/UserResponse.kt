package ru.polygalov.repositorysearch.data.remote

import androidx.annotation.Nullable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "user")
data class UserResponse (
    @PrimaryKey @field:SerializedName("id") val total: Long,
    @field:SerializedName("login") val login: String,
    @field:SerializedName("avatar_url") val avatar_url: String,
    @field:SerializedName("created_at") val created_at: String,
    @field:SerializedName("location") val location: String?
)