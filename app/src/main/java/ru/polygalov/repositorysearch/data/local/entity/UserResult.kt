package ru.polygalov.repositorysearch.data.local.entity

import androidx.lifecycle.LiveData
import ru.polygalov.repositorysearch.data.remote.UserResponse

data class UserResult (
    val data: LiveData<UserResponse>,
    val networkErrors: LiveData<String>
)