package ru.polygalov.repositorysearch.data

import android.util.Log
import androidx.lifecycle.MutableLiveData
import ru.polygalov.repositorysearch.data.local.GithubLocalCache
import ru.polygalov.repositorysearch.data.local.entity.UserReposResult
import ru.polygalov.repositorysearch.data.local.entity.UserResult
import ru.polygalov.repositorysearch.data.remote.GithubService
import ru.polygalov.repositorysearch.data.remote.getUser
import ru.polygalov.repositorysearch.data.remote.getUserRepos

class DetailsRepository(
    private val service: GithubService,
    private val cache: GithubLocalCache
) {
    private var lastRequestedPage = 1
    private val networkErrors = MutableLiveData<String>()
    private var isRequestInProgress = false

    fun searchUser(username: String): UserResult {
        Log.d("GithubRepository", "New query: $username")
        requestAndSaveUserData(username)

        val data = cache.getUser(username)

        return UserResult(data, networkErrors)
    }

    private fun requestAndSaveUserData(query: String) {
        getUser(service, query,  { user ->
            cache.insertUser(user) {

            }
        }, { error ->
            networkErrors.postValue(error)
        })
    }

    fun searchUsersRepos(username: String): UserReposResult {
        Log.d("GithubRepository", "New query: $username")
        lastRequestedPage = 1
        requestAndSaveUsersReposData(username)

        val data = cache.getUserRepos(username)

        return UserReposResult(data, networkErrors)
    }

    private fun requestAndSaveUsersReposData(query: String) {
        if (isRequestInProgress) return

        isRequestInProgress = true
        getUserRepos(service, query,  { userRepos ->
            cache.insertUserRepos(userRepos) {
                lastRequestedPage++
                isRequestInProgress = false
            }
        }, { error ->
            networkErrors.postValue(error)
            isRequestInProgress = false
        })
    }

}